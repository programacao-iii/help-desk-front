# Help Desk - Front
Sistema de cadastro e gestão de tickets.

Backend: [Help Desk - Api](https://bitbucket.org/prolo/help-desk-api/src/master/)

<hr/>

## Informações

Frontend desenvolvido em AngularJs

Esse frontend tem rotas para:
- Listagem de tickets
- Cadastro de ticket
- Visualização de detalhes de um ticket

<br>

<hr>


## Listagem de tickets:

![listagem_screenshot](screenshot_1.png)

Nessa tela o usuário poderá visualizar uma listagem de tickets, e também consegue pesquisar tickets pelo nome de quem registrou o chamado ou de quem é o responsável pelo chamado.

A partir dessa tela o usuário pode clicar em um ticket para visualizar os detalhes ou criar um novo ticket.

O usuário também poderá fechar os tickets através da listagem.

<hr>

## Cadastro de tickets:

![listagem_screenshot](screenshot_2.png)

Nessa tela o usuário pode preencher um formulário com as informações do ticket que deseja criar. 

Depois de clicar em salvar, o usuário será redirecionado para a tela de detalhes desse ticket.

<hr>

## Deatlhes do ticket:

![listagem_screenshot](screenshot_3.png)

Nessa tela o usuário pode visualizar os detalhes de um ticket, bem como realizar o fechamento do ticket.

Aqui o usuário também poderá retornar a listagem de tickets.

<br><br>
# Angular Info

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.7.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
