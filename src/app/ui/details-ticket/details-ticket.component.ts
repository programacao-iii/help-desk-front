import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Ticket } from 'src/app/model/ticket.model';
import { TicketService } from 'src/app/service/ticket.service';

@Component({
  selector: 'app-details-ticket',
  templateUrl: './details-ticket.component.html',
  styleUrls: ['./details-ticket.component.css']
})
export class DetailsTicketComponent implements OnInit {

  ticket: Ticket = new Ticket()

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private ticketService: TicketService
  ) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.findTicket(params.id)
    })
  }

  findTicket(id: number) {
    this.ticketService.findById(id).subscribe(
      response => this.ticket = response.result
    )
  }

  changeStatus() {
    if (this.ticket == null || this.ticket.status == 2) return;

    this.ticket.status += 1
    this.ticketService.updateTicket(this.ticket).subscribe(
      _ => this.findTicket(this.ticket.id)
    ) 
  }

  ticketActionText = () => this.ticket.status == 0 ? "Iniciar" : "Fechar"

  ticketStatusText = () => this.ticket.status == 0 ? 
                            "Aberto" : this.ticket.status == 1 ? 
                              "Em Andamento" : "Fechado"

  back = () => this.router.navigateByUrl("/")

}