import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { DefaultResponse } from 'src/app/model/response.model';
import { Ticket } from 'src/app/model/ticket.model';
import { TicketService } from 'src/app/service/ticket.service';

@Component({
  selector: 'app-list-tickets',
  templateUrl: './list-tickets.component.html',
  styleUrls: ['./list-tickets.component.css']
})
export class ListTicketsComponent implements OnInit {

  tickets: Ticket[] = []

  formSearch = this.formBuilder.group({
    searchType: [1, [Validators.required]],
    searchText: ['', [Validators.required]]
  })

  constructor(private ticketService: TicketService,
              private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.findTickets()
  }

  findTickets() {
    this.ticketService.findAll().subscribe(
      response => this.onSearchResult(response)
    )
  }

  search() {
    if (this.searchType.value == 1) {
      this.ticketService.findByNameIssuerOrNameResponder(this.searchText.value).subscribe(
        response => this.onSearchResult(response)
      )
    } else if (this.searchType.value == 2) {
      this.ticketService.findByNameResponder(this.searchText.value).subscribe(
        response => this.onSearchResult(response)
      )
    } else if (this.searchType.value == 3) {
      this.ticketService.findByNameIssuer(this.searchText.value).subscribe(
        response => this.onSearchResult(response)
      )
    }
  }

  onSearchResult(response: DefaultResponse<Ticket[]>) {
    console.log(response)
    if (response != null) {
      this.tickets = response.result
    } else {
      this.tickets = null
    }
  }

  clearSearch() {
    this.searchText.setValue("")
    this.searchType.setValue(1)
    this.findTickets()
  }

  closeTicket(ticket: Ticket) {
    if (ticket.status == 2) return; 
    ticket.status += 1
    this.ticketService.updateTicket(ticket).subscribe()
  }

  ticketStatusText = (status: number) => status == 0 ? 
                                          "Aberto" : status == 1 ? 
                                            "Em Andamento" : "Fechado"

  ticketActionText = (status: number) => status == 0 ? "Iniciar" : "Fechar"

  get searchType() {
    return this.formSearch.controls.searchType
  }

  get searchText() {
    return this.formSearch.controls.searchText
  }

}
