import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RegisterTicket } from 'src/app/model/ticket.model';
import { TicketService } from 'src/app/service/ticket.service';

@Component({
  selector: 'app-register-ticket',
  templateUrl: './register-ticket.component.html',
  styleUrls: ['./register-ticket.component.css']
})
export class RegisterTicketComponent {

  formTicket = this.formBuilder.group({
    title: ['', Validators.required],
    name_issuer: ['', Validators.required],
    email_issuer: ['', [Validators.required, Validators.email]],
    name_responder: ['', Validators.required],
    email_responder: ['', [Validators.required, Validators.email]],
    description: ['', Validators.required]
  })

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private ticketService: TicketService) { }

  save() { 
    let ticket = new RegisterTicket(
      this.name_issuer.value,
      this.email_issuer.value,
      this.name_responder.value,
      this.email_responder.value,
      this.title.value,
      this.description.value
    )

    this.ticketService.createTicket(ticket).subscribe(
      response => {
        this.router.navigate(["/details-ticket"], {queryParams: {id: response.result.id}})
      }
    )
  }

  cancel() {
    this.router.navigateByUrl("/")
  }

  get title() {
    return this.formTicket.controls.title
  }

  get name_issuer() {
    return this.formTicket.controls.name_issuer
  }

  get email_issuer() {
    return this.formTicket.controls.email_issuer
  }

  get name_responder() {
    return this.formTicket.controls.name_responder
  }

  get email_responder() {
    return this.formTicket.controls.email_responder
  }

  get description() {
    return this.formTicket.controls.description
  }
}