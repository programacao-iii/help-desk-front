export class Ticket {
    public id             : number
    public name_issuer    : string
    public email_issuer   : string
    public name_responder : string
    public email_responder: string
    public title          : string
    public description    : string
    public open_date      : Date
    public status         : number
    public close_date     : Date
}
export class RegisterTicket {
    public name_issuer    : string
    public email_issuer   : string
    public name_responder : string
    public email_responder: string
    public title          : string
    public description    : string

    constructor(
        name_issuer    : string,
        email_issuer   : string,
        name_responder : string,
        email_responder: string,
        title          : string,
        description    : string
    ) 
    {
        this.name_issuer     = name_issuer
        this.email_issuer    = email_issuer
        this.name_responder  = name_responder
        this.email_responder = email_responder
        this.title           = title
        this.description     = description
    }
}
