import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms'
import { DetailsTicketComponent } from './ui/details-ticket/details-ticket.component';
import { AppComponent } from './ui/home/app.component';
import { ListTicketsComponent } from './ui/list-tickets/list-tickets.component';
import { RegisterTicketComponent } from './ui/register-ticket/register-ticket.component';

@NgModule({
  declarations: [
    AppComponent,
    RegisterTicketComponent,
    ListTicketsComponent,
    DetailsTicketComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
