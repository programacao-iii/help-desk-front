import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { RegisterTicket, Ticket } from "../model/ticket.model";
import { DefaultResponse } from "../model/response.model";

const URL_BASE = "http://localhost:8080/ticket/"
const URL_FIND_BY_NAME_ISSUER_OR_NAME_RESPONDER = URL_BASE + "/byNameIssuerOrNameResponder?name="
const URL_FIND_BY_NAME_ISSUER = URL_BASE + "/byNameIssuer?name="
const URL_FIND_BY_NAME_RESPONDER = URL_BASE + "/byNameResponder?name="
const headers = { 'Content-Type': 'application/json'}

@Injectable({
    providedIn: 'root'
})
export class TicketService {

    constructor(private http: HttpClient) {

    }

    findAll(): Observable< DefaultResponse<Ticket[]> > {
        return this.http.get< DefaultResponse<Ticket[]> >(URL_BASE)
    }

    findByNameIssuerOrNameResponder(name: string): Observable< DefaultResponse<Ticket[]>> {
        return this.http.get< DefaultResponse<Ticket[]> >(
            URL_FIND_BY_NAME_ISSUER_OR_NAME_RESPONDER + name
            )
    }

    findByNameIssuer(name: string): Observable< DefaultResponse<Ticket[]>> {
        return this.http.get< DefaultResponse<Ticket[]> >(URL_FIND_BY_NAME_ISSUER + name)
    }

    findByNameResponder(name: string): Observable< DefaultResponse<Ticket[]>> {
        return this.http.get< DefaultResponse<Ticket[]> >(URL_FIND_BY_NAME_RESPONDER + name)
    }

    findById(id: number): Observable< DefaultResponse<Ticket>> {
        return this.http.get< DefaultResponse<Ticket> >(URL_BASE + id)
    }
    
    createTicket(ticket: RegisterTicket): Observable< DefaultResponse<Ticket>> {
        return this.http.post< DefaultResponse<Ticket> >(URL_BASE, JSON.stringify(ticket), { headers })
    }

    updateTicket(ticket: Ticket): Observable< DefaultResponse<boolean>> {
        return this.http.put< DefaultResponse<boolean> >(URL_BASE, JSON.stringify(ticket), { headers })
    }
}